
from setuptools import setup, find_packages

setup(
    name='autobahn2use',
    version='0.0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An example python package',
    long_description=open('README.txt').read(),
    install_requires=[
        'autobahn',
    ],
    url='https://github.com/autobahn2use/library4python',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com'
)
